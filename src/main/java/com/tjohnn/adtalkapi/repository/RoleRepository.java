package com.tjohnn.adtalkapi.repository;

import com.tjohnn.adtalkapi.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByName(String name);
}
