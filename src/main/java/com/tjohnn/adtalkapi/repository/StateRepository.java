package com.tjohnn.adtalkapi.repository;

import com.tjohnn.adtalkapi.model.State;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StateRepository extends JpaRepository<State, Integer> {


}
