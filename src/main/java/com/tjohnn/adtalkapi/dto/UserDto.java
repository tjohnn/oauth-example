package com.tjohnn.adtalkapi.dto;

import com.tjohnn.adtalkapi.model.Role;
import com.tjohnn.adtalkapi.model.User;
import org.springframework.boot.autoconfigure.security.oauth2.resource.AuthoritiesExtractor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;

public class UserDto {

    private String username;
    private String password;

    private List<Role> roles;

    public UserDto(User user) {

        username = user.getEmail();
        password = user.getPassword();
        roles = user.getRoles();

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
