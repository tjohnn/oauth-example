package com.tjohnn.adtalkapi.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.requestMatchers()
                .antMatchers("/**")
                .and()
                .authorizeRequests()
                .antMatchers( "/register", "/oauth/token").permitAll()
                .antMatchers("/api/v1/root/**").access("hasRole('ROOT')")
                .antMatchers("/api/v1/admin/**", "/states").access("hasRole('ADMIN') or hasRole('ROOT')")
                .anyRequest().authenticated();
    }
}
