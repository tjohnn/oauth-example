package com.tjohnn.adtalkapi.service;

import com.tjohnn.adtalkapi.model.State;
import com.tjohnn.adtalkapi.repository.StateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class StateService {

    @Autowired
    StateRepository stateRepository;

    public List<State> getAllStates(){
        return stateRepository.findAll();
    }

    public Optional<State> getStateById(int id){
        return  stateRepository.findById(id);
    }


}
