package com.tjohnn.adtalkapi.service;

import com.tjohnn.adtalkapi.model.Role;
import com.tjohnn.adtalkapi.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    @Autowired
    RoleRepository roleRepository;

    public Role findByName(String name){
        return roleRepository.findByName(name);
    }

}
