package com.tjohnn.adtalkapi.service;

import com.tjohnn.adtalkapi.model.User;
import com.tjohnn.adtalkapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public User save(User user){
         return userRepository.save(user);
    }

}
