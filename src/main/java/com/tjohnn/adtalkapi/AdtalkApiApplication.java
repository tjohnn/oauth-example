package com.tjohnn.adtalkapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdtalkApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdtalkApiApplication.class, args);
	}
}
