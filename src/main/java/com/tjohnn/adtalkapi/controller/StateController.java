package com.tjohnn.adtalkapi.controller;

import com.tjohnn.adtalkapi.model.Role;
import com.tjohnn.adtalkapi.model.State;
import com.tjohnn.adtalkapi.model.User;
import com.tjohnn.adtalkapi.service.RoleService;
import com.tjohnn.adtalkapi.service.StateService;
import com.tjohnn.adtalkapi.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
public class StateController {

    private static final Logger log = LoggerFactory.getLogger(StateController.class);

    @Autowired
    UserService userService;


    @Autowired
    StateService stateService;

    @Autowired
    RoleService roleService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @GetMapping("states")
    public ResponseEntity<List<State>> getStates(){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        return new ResponseEntity<>(stateService.getAllStates(), headers, HttpStatus.OK);
    }

    @PostMapping("register_")
    public ResponseEntity<?> registerUser_(@RequestBody User user){
        user.setRoles(Arrays.asList(roleService.findByName("USER")));
        String password = passwordEncoder.encode(user.getPassword());
        log.debug(password);
        user.setPassword(password);
        user = userService.save(user);
        log.debug(user.toString());

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("register")
    public ResponseEntity<?> registerUser(@RequestParam("first_name") String firstName,
                                          @RequestParam("last_name") String lastName,
                                          @RequestParam("email") String email,
                                          @RequestParam(value = "phone_number") String phoneNumber,
                                          @RequestParam("password") String password
                                          ){
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setPhoneNumber(phoneNumber);
        user.setPassword(passwordEncoder.encode(password));
        user.setRoles(Arrays.asList(roleService.findByName("USER")));

        log.info(password);

        user = userService.save(user);
        log.info(user.toString());

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

}
