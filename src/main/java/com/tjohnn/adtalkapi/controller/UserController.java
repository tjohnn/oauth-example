package com.tjohnn.adtalkapi.controller;

import com.tjohnn.adtalkapi.model.Role;
import com.tjohnn.adtalkapi.model.User;
import com.tjohnn.adtalkapi.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Arrays;

@RestController("api/v1")
public class UserController {

    Logger logger = LoggerFactory.getLogger(UserController.class.getName());

    @Autowired
    UserService userService;

    @GetMapping("users/me")
    public Principal getPrincipal(Principal principal){
        return principal;
    }



}
